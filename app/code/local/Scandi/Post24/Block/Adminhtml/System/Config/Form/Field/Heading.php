<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Block_Adminhtml_System_Config_Form_Field_Heading
 */
class Scandi_Post24_Block_Adminhtml_System_Config_Form_Field_Heading extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{
    /**
     * Render element html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     *
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $note = ($element->getComment() && $element->getComment() != '')
            ? sprintf('<p class="note"><span>%s</span></p>', $element->getComment())
            : '';

        return sprintf(
            '<tr class="system-fieldset-sub-head" id="row_%s"><td colspan="5"><h4>%s</h4>%s</td></tr>',
            $element->getHtmlId(), $element->getLabel(), $note
        );
    }
}
