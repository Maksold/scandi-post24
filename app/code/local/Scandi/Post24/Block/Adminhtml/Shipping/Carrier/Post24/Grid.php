<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Block_Adminhtml_Shipping_Carrier_Post24_Grid
 */
class Scandi_Post24_Block_Adminhtml_Shipping_Carrier_Post24_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Website filter
     *
     * @var int
     */
    protected $_websiteId;
    /**
     * Condition filter
     *
     * @var string
     */
    protected $_conditionName;

    /**
     * Define grid properties
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('scandiPost24Grid');
        $this->_exportPageSize = 10000;
    }

    /**
     * Prepare table columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'name', array(
                'header' => Mage::helper('adminhtml')->__('Name'),
                'index'  => 'name',
            )
        );

        $this->addColumn(
            'city', array(
                'header' => Mage::helper('adminhtml')->__('City'),
                'index'  => 'city',
            )
        );

        $this->addColumn(
            'address', array(
                'header' => Mage::helper('adminhtml')->__('Address'),
                'index'  => 'address',
            )
        );

        $this->addColumn(
            'index', array(
                'header' => Mage::helper('adminhtml')->__('Index'),
                'index'  => 'index',
            )
        );

        $this->addColumn(
            'place', array(
                'header' => Mage::helper('adminhtml')->__('Place'),
                'index'  => 'place',
            )
        );

        $this->addColumn(
            'created_at', array(
                'header' => Mage::helper('adminhtml')->__('Create At'),
                'index'  => 'created_at',
            )
        );

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('scandi_post24/terminals_collection');
        $collection->setWebsiteFilter($this->getWebsiteId());

        $this->setCollection($collection);

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    /**
     * Retrieve current website id
     *
     * @return int
     */
    public function getWebsiteId()
    {
        if (is_null($this->_websiteId)) {
            $this->_websiteId = Mage::app()->getWebsite()->getId();
        }

        return $this->_websiteId;
    }

    /**
     * Set current website
     *
     * @param $websiteId
     *
     * @return $this
     */
    public function setWebsiteId($websiteId)
    {
        $this->_websiteId = Mage::app()->getWebsite($websiteId)->getId();

        return $this;
    }
}