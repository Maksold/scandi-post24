<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Scandi_Post24 Setup install script
 */

$installer = $this;

$installer->run(
    "
  DROP TABLE IF EXISTS {$this->getTable('scandi_post24/terminals')};
  CREATE TABLE IF NOT EXISTS {$this->getTable('scandi_post24/terminals')} (
    `terminal_id` int(11) unsigned NOT NULL auto_increment,
    `store_id` int(11) NOT NULL,
    `name` varchar(254) NOT NULL,
    `city` varchar(254) NOT NULL,
    `address` varchar(254) NOT NULL,
    `index` varchar(254) NOT NULL,
    `place` varchar(254) NOT NULL,
    `created_at` datetime NULL,
    PRIMARY KEY (`terminal_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);

$installer->getConnection()->addConstraint(
    'FK_SCANDI_POST24_TERMINALS_STORE_ID_ITEM',
    $installer->getTable('scandi_post24/terminals'),
    'store_id',
    $installer->getTable('core_store'),
    'store_id',
    'cascade',
    'cascade'
);