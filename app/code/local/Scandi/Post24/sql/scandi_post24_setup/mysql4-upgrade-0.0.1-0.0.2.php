<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Scandi_Post24 Setup upgrade script
 */

$installer = $this;
$field = 'post24_boxes';

$this->getConnection()->addColumn($this->getTable('sales_flat_quote'), $field, 'int(10) unsigned');
$this->getConnection()->addColumn($this->getTable('sales_flat_order'), $field, 'int(10) unsigned');