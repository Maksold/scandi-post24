<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Adminhtml_System_Config_Backend_Shipping_Post24
 */
class Scandi_Post24_Model_Adminhtml_System_Config_Backend_Shipping_Post24 extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
        Mage::getModel('scandi_post24/import')->uploadAndImport($this);
    }
}
