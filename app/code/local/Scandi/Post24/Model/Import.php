<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Import
 */
class Scandi_Post24_Model_Import
{
    /**
     * Data import
     *
     * @param                       $data
     * @param Mage_Core_Model_Store $store
     *
     * @return $this
     */
    public function import($data, Mage_Core_Model_Store $store)
    {
        if (count($data)) {
            /**
             * Delete collection by store
             */
            $this->_getTerminals()->deleteCollectionByStore($store);
            foreach ($data AS $item) {
                $model = $this->_getTerminals();
                /**
                 * Add new collection
                 */
                $model->setData($item);
                $model->setStoreId($store->getId());
                $model->save();
            }
        }

        return $this;
    }

    /**
     * Upload csv file and upload
     *
     * @param Varien_Object $object
     *
     * @return $this
     */
    public function uploadAndImport(Varien_Object $object)
    {
        if (!isset($_FILES['groups']['tmp_name']['scandipost24']['fields']['json_import']['value'])
            || empty($_FILES['groups']['tmp_name']['scandipost24']['fields']['json_import']['value'])
        ) {
            return $this;
        }

        $jsonFile = $_FILES['groups']['tmp_name']['scandipost24']['fields']['json_import']['value'];
        $website = Mage::app()->getWebsite($object->getScopeId());

        $io = new Varien_Io_File();
        $info = pathinfo($jsonFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        try {
            $remoteData = '';
            while (false !== ($jsonLine = $io->streamRead())) {
                if (empty($jsonLine)) {
                    continue;
                }
                $remoteData .= $jsonLine;
            }

            foreach ($website->getStores() As $store) {
                $data = $this->_getRemoteParser()->setData($remoteData)->setStore($store)->getRemoteDataArray();
                if ($data !== false && count($data)) {
                    /**
                     * Import Data
                     */
                    $this->import($data, $store);
                }
            }
        } catch (Exception $err) {
            $error = Mage::helper('scandi_post24')->__('File has not been imported. ERROR: %s', $err->getMessage());
            Mage::throwException($error);
        }

        $io->streamClose();

        return $this;
    }

    /**
     * Retrieve Scandi_Post24_Model_Remote_Parser
     *
     * @return Scandi_Post24_Model_Remote_Parser
     */
    protected function _getRemoteParser()
    {
        return Mage::getModel('scandi_post24/remote_parser');
    }

    /**
     * Retrieve Scandi_Post24_Model_Terminals
     *
     * @return Scandi_Post24_Model_Terminals
     */
    protected function _getTerminals()
    {
        return Mage::getModel('scandi_post24/terminals');
    }
}