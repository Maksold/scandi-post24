<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Cron
 */
class Scandi_Post24_Model_Cron
{
    CONST XML_PATH_ENDPOINT_CRON_ENABLED = 'carriers/scandipost24/cron_enabled';

    /**
     * Check is cron job enabled
     *
     * @param Mage_Core_Model_Store $store
     *
     * @return mixed
     */
    protected function isCronEnabled(Mage_Core_Model_Store $store)
    {
        return Mage::getStoreConfig(self::XML_PATH_ENDPOINT_CRON_ENABLED, $store->getId());
    }

    /**
     * Run place update
     *
     * @return $this
     */
    public function runPlacesUpdate()
    {
        foreach (Mage::app()->getStores() As $store) {
            /**
             * @var $store Mage_Core_Model_Store
             */
            $url = Mage::getStoreConfig(Scandi_Post24_Model_Remote_Service::XML_PATH_ENDPOINT_URL, $store->getId());
            if ($url && $url != '' && $this->isCronEnabled($store)) {
                $this->_getImport()->import($this->_getTerminals()->loadFromRemote($store), $store);
            }
        }

        return $this;
    }

    /**
     * Retrieve Scandi_Post24_Model_Terminals model
     *
     * @return Scandi_Post24_Model_Terminals
     */
    protected function _getTerminals()
    {
        return Mage::getModel('scandi_post24/terminals');
    }

    /**
     * Retrieve Scandi_Post24_Model_Import model
     *
     * @return Scandi_Post24_Model_Import
     */
    protected function _getImport()
    {
        return Mage::getModel('scandi_post24/import');
    }

}