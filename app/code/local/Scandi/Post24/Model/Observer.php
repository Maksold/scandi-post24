<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Observer
 */
class Scandi_Post24_Model_Observer
{
    /**
     * Save boxes count in quote
     *
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function saveBoxesInQuote(Varien_Event_Observer $observer)
    {
        /**
         * @var $quote Mage_Sales_Model_Quote
         */
        $quote = $observer->getEvent()->getQuote();
        $address = $quote->getShippingAddress();
        $shippingMethod = $address->getShippingMethod();
        if (!$address->getShippingRateByCode($shippingMethod)) {
            return $this;
        }
        $description = unserialize($address->getShippingRateByCode($shippingMethod)->getData('method_description'));

        if (is_array($description) && isset($description['post24_boxes'])) {
            $quote->setData('post24_boxes', $description['post24_boxes']);
        }

        return $this;
    }

    /**
     * Save boxes count in order as shipping_description
     *
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function extendShippingDescription(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        /**
         * @var $order Mage_Sales_Model_Order
         */

        if ($order->getShippingMethod()) {
            try {

                $index = str_replace(Scandi_Post24_Model_Carrier_Post24::_SHIPPING_METHOD_CODE . '_', '', $order->getShippingMethod());
                $terminal = Mage::getSingleton('scandi_post24/terminals')->loadByIndex($index);

                if ($terminal && $terminal->getId()) {
                    $description = sprintf(
                        "%s - %s, %s, %s (%s)",
                        Mage::helper('scandi_post24')->getShippingTitle(),
                        $terminal->getCity(),
                        $terminal->getAddress(),
                        $terminal->getPlace(),
                        $order->getData('post24_boxes')
                    );
                    $order->setShippingDescription($description);
                }

            } catch (Exception $error) {
                Mage::logException($error);
            }
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function salesConvertOrderItemToQuoteItem(Varien_Event_Observer $observer)
    {
        /* @var $quoteItem Mage_Sales_Model_Quote_Item */
        $quoteItem = $observer->getData('quote_item');

        /**
         * This is a fix for magento core bug - when reorder is being done in BE magento does not load necessary
         * attributes for child quote item products as specified under global/sales/quote/item/product_attributes
         * Therefore we load it manually. This method is used only in BE so it doesn't affect FE performance.
         */
        if ($quoteItem->getParentItem() && !$quoteItem->getProduct()->getData('is_loaded')) {
            $product = $quoteItem->getProduct()->load($quoteItem->getProductId());
            $product->setData('is_loaded', 1);
            $quoteItem->setProduct($product);
        }
    }
}