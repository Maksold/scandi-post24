<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Carrier_Post24
 */
class Scandi_Post24_Model_Carrier_Post24 extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    /**
     * Shipping method code
     */
    const _SHIPPING_METHOD_CODE = 'scandipost24';
    /**
     * Product weight attribute
     */
    const _PRODUCT_ATTRIBUTE_WEIGHT = 'weight';
    /**
     *  Quote item size field
     */
    const _PRODUCT_ATTRIBUTE_SIZE = 'dimensions';
    /**
     * Product free shipping attribute
     */
    const _PRODUCT_ATTRIBUTE_FREE_SHIPPING = 'free_shipping';
    /**
     * Volume math type rectangular
     */
    const _SIZE_TYPE_RECTANGULAR = 1;
    /**
     * Errors types
     */
    const _ERROR_TYPE_VOLUME = 1;
    const _ERROR_TYPE_SIZE_EMPTY = 2;

    /**
     * @var string
     */
    protected $_code = self::_SHIPPING_METHOD_CODE;
    /**
     * @var $_request Mage_Shipping_Model_Rate_Request
     */
    protected $_request = null;
    /**
     * @var null
     */
    protected $_terminals = null;
    /**
     * @var array
     */
    static $sizeHeader = array('x', 'y', 'z', 'w');
    /**
     * @var int
     */
    protected $_error = 0;

    /**
     * Collected shipping method rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     *
     * @return bool|Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        $this->_request = $request;

        /* @var $result Mage_Shipping_Model_Rate_Result */
        $result = Mage::getModel('shipping/rate_result');

        $this->_getShippingRate($result);

        return $result;
    }

    /**
     * Return rates
     *
     * @param $result
     *
     * @return Mage_Shipping_Model_Rate_Result_Error|Mage_Shipping_Model_Rate_Result_Method
     */
    protected function _getShippingRate(&$result)
    {
        $error = false;

        /**
         * Validate free shipping price
         */
        if ($this->getConfigFlag('free_shipping_enable')
            && $this->getConfigData('free_shipping_subtotal') <= $this->_request->getBaseSubtotalInclTax()
        ) {
            $this->_request->setFreeShipping(true);
        }

        $shippingPrice = $this->getFinalPriceWithHandlingFee($this->getConfigData('price'));

        /*
        * Calculate shipping price, if price isn't fixed
        */
        if (!$this->getConfigFlag('fixed_price_enable')) {

            /**
             * Validate boxes
             */
            $boxSize = $this->_getBoxSize();
            $freePlace = $this->_mathVolume($boxSize);
            $freeWeight = $boxSize['w'];
            $boxes = 1;

            foreach ($this->_request->getAllItems() as $item) {

                /* @var $item Mage_Sales_Model_Quote_Item */
                if ($item->getProductType() == 'configurable') {
                    $children = $item->getChildren();
                    $item = reset($children);
                }

                if ($item->getProduct()->isVirtual() || $item->getParentItemId()) {
                    continue;
                }

                if ($this->getConfigFlag('free_shipping_enable') && $item->getProduct()->getData(self::_PRODUCT_ATTRIBUTE_FREE_SHIPPING) == 1) {
                    $this->_request->setFreeShipping(true);
                }

                $itemSize = $this->_getItemSize($item);
                $itemVolume = $this->_mathVolume($itemSize);

                if (!$itemSize['x'] || !$itemSize['y'] || !$itemSize['z'] || !$itemSize['w']) {
                    $this->_error = self::_ERROR_TYPE_SIZE_EMPTY;
                    break;
                }

                if (!$this->_itemFits($itemSize, $boxSize)) {
                    $this->_error = self::_ERROR_TYPE_VOLUME;
                    break;
                }

                if ($itemSize['w'] > $boxSize['w']) {
                    $this->_error = self::_ERROR_TYPE_VOLUME;
                    break;
                }

                /**
                 * Count boxes size
                 */
                for ($i = 0; $i < $item->getQty(); $i++) {
                    $limit = false;

                    if ($freePlace - $itemVolume < 0) {
                        $limit = true;
                    }

                    if ($freeWeight - $itemSize['w'] < 0) {
                        $limit = true;
                    }

                    if ($limit) {
                        $freePlace = $this->_mathVolume($boxSize);
                        $freeWeight = $boxSize['w'];
                        $boxes++;
                    }

                    $freePlace -= $itemVolume;
                    $freeWeight -= $itemSize['w'];
                }
            }

            if ($boxes > 1) {
                $shippingPrice += $this->getConfigData('next_price') * ($boxes - 1);
            }

            /**
             * Set wrong product(s) size error message
             */
            switch ($this->_error) {
                case (self::_ERROR_TYPE_VOLUME):
                case (self::_ERROR_TYPE_SIZE_EMPTY):
                    $result->append($this->_setErrorMethod($this->getConfigData('wrong_size_error')));
                    return $this;
                    break;
                default;
                    break;
            }
        }
        else {
            $boxes = 1;
        }

        if ($this->_request->getFreeShipping() === true) {
            $shippingPrice = '0.00';
        }

        $terminals = $this->_getTerminalByStore();
        if (count($terminals)) {
            foreach ($terminals AS $terminal) {
                /**
                 * @var $terminal Scandi_Post24_Model_Terminals
                 */
                $method = Mage::getModel('shipping/rate_result_method');
                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));

                $method->setMethod($terminal->getIndex());
                $method->setMethodTitle($this->combineTerminalName($terminal) . (($boxes > 1) ? ' x ' . $boxes : ''));
                $method->setMethodDescription(serialize(array('post24_boxes' => $boxes)));

                $method->setPrice($shippingPrice);
                $method->setCost($shippingPrice);

                $result->append($method);
            }
        }

        return $this;
    }

    /**
     * Return terminal index from code
     *
     * @param $terminalCode
     *
     * @return mixed
     */
    protected function _getTerminalIndex($terminalCode)
    {
        return str_replace($this->_code . '_', '', $terminalCode);
    }

    /**
     * Create terminal code
     *
     * @param $index
     *
     * @return string
     */
    protected function _createTerminalCode($index)
    {
        return $this->_code . '_' . $index;
    }

    /**
     * Set Shipping Method Error
     *
     * @param string $message
     *
     * @return Mage_Shipping_Model_Rate_Result_Error
     */
    protected function _setErrorMethod($message = '')
    {
        $error = Mage::getModel('shipping/rate_result_error');
        $error->setCarrier($this->_code);
        $error->setCarrierTitle($this->getConfigData('title'));

        $error->setMethod($this->_code);
        $error->setMethodTitle($this->getConfigData('title'));

        $error->setErrorMessage($message);

        return $error;
    }

    /**
     * Return terminals collection by store
     *
     * @return null|Scandi_Post24_Model_Resource_Terminals_Collection
     */
    protected function _getTerminalByStore()
    {
        if (is_null($this->_terminals)) {

            $this->_terminals = Mage::getModel('scandi_post24/terminals')->getCollection()
                ->addStoreFilter($this->_getStore())
                ->setOrder('city', 'asc')
                ->load();

        }

        return $this->_terminals;
    }

    /**
     * @return Mage_Core_Model_Store
     */
    protected function _getStore()
    {
        if ($this->_request) {
            return Mage::app()->getStore($this->_request->getStoreId());
        }
        return Mage::app()->getStore();
    }

    /**
     * Return allowed methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        $methods = array();

        $terminals = $this->_getTerminalByStore();
        if (count($terminals)) {
            foreach ($terminals AS $terminal) {
                /**
                 * @var $terminal Scandi_Post24_Model_Terminals
                 */
                $code = $this->_createTerminalCode($terminal->getIndex());
                $methods[$code] = $this->combineTerminalName($terminal);
            }
        }

        $methods[$this->_code] = $this->getConfigData('title');

        return $methods;
    }

    /**
     * Combine terminal name
     *
     * @param $terminal
     *
     * @return string
     */
    protected function combineTerminalName(Scandi_Post24_Model_Terminals $terminal)
    {
        $addressWithoutCity = str_replace($terminal->getCity(), '', $terminal->getAddress());
        $result = $terminal->getCity() . ', ' . $addressWithoutCity . ', ' . $terminal->getName();
        $result = str_replace(array(',,', ', , ', ' ,'), array(',', ', ', ','), $result);

        return $result;
    }

    /**
     * Return box size
     *
     * @return array
     */
    protected function _getBoxSize()
    {
        $sizes = explode('x', $this->getConfigData('box_size'));
        $sizes[] = $this->getConfigData('box_max_weight');

        return array_combine(self::$sizeHeader, $sizes);
    }

    /**
     * Return quote item size
     *
     * @param Mage_Sales_Model_Quote_Item $item
     *
     * @return array
     */
    protected function _getItemSize($item)
    {
        $dimensions = $item->getProduct()->getData(self::_PRODUCT_ATTRIBUTE_SIZE);
        if (!$dimensions || $dimensions == 0) {
            $sizes = array(0, 0, 0);
        } else {
            $sizes = (array)explode('x', $dimensions);

            foreach ($sizes AS $key => $size) {
                $sizes[$key] = $size + (int)$this->getConfigData('box_padding');
            }
        }
        $sizes[] = $item->getData(self::_PRODUCT_ATTRIBUTE_WEIGHT);

        return array_combine(self::$sizeHeader, $sizes);
    }

    /**
     * Math item volume
     *
     * @param     $size
     * @param int $type
     *
     * @return int
     */
    protected function _mathVolume($size, $type = 1)
    {
        $volume = 0;
        switch ($type) {
            case self::_SIZE_TYPE_RECTANGULAR:
                $volume = $size['x'] * $size['y'] * $size['z'];
                break;
            default:
                break;
        }

        return $volume;
    }

    /**
     * Items can be rotated - therefore we need to check all rotation combinations to decide whether item will
     * fit in the box
     *
     * @param  array $itemSize
     * @param  array $boxSize
     * @return bool
     */
    public function _itemFits($itemSize, $boxSize)
    {
        unset($itemSize['w']);
        $boxSizePermutations = array();
        $this->_getPermutations(array_values($itemSize), array(), $boxSizePermutations);
        foreach ($boxSizePermutations as $prm) {
            if ($prm[0] <= $boxSize['x'] && $prm[1] <= $boxSize['y'] && $prm[2] <= $boxSize['z']) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get all permutations of array items and store them in $permutations parameter
     *
     * @param  array $items
     * @param  array $perms
     * @param  array $permutations
     * @return array
     */
    protected function _getPermutations($items, $perms = array(), &$permutations)
    {
        if (empty($items)) {
            $permutations[] = $perms;
        } else {
            for ($i = count($items) - 1; $i >= 0; --$i) {
                $newitems = $items;
                $newPerms = $perms;
                list($foo) = array_splice($newitems, $i, 1);
                array_unshift($newPerms, $foo);
                $this->_getPermutations($newitems, $newPerms, $permutations);
            }
        }
    }
}