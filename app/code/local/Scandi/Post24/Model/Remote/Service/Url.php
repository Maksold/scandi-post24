<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Remote_Service_Url
 */
class Scandi_Post24_Model_Remote_Service_Url extends Scandi_Post24_Model_Remote_Service_Abstract
{
    /**
     * Return data
     *
     * @return string
     */
    public function get()
    {
        return file_get_contents($this->_url);
    }
}