<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Remote_Service_Abstract
 */
abstract class Scandi_Post24_Model_Remote_Service_Abstract
{
    /**
     * @var null
     */
    protected $_url = null;

    /**
     * Setter for $_url
     *
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url = '')
    {
        $this->_url = $url;

        return $this;
    }

    /**
     * Abstract method to return data
     *
     * @return string
     */
    abstract public function get();
}