<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Remote_Service
 */
class Scandi_Post24_Model_Remote_Service
{
    /**
     *
     */
    CONST XML_PATH_ENDPOINT_TYPE = 'carriers/scandipost24/endpoint_type';
    /**
     *
     */
    CONST XML_PATH_ENDPOINT_URL = 'carriers/scandipost24/endpoint_url';

    /**
     * @var Scandi_Post24_Model_Remote_Service_Abstract
     */
    protected $_service = null;
    /**
     * @var Mage_Core_Model_Store
     */
    protected $_store = null;

    /**
     *
     */
    public function __construct()
    {
        $this->_setService();
    }

    /**
     * Set service model
     *
     * @return $this
     */
    protected function _setService()
    {
        $type = $this->_getType();

        if (!$type) {
            return $this;
        }

        $this->_service = Mage::getModel('scandi_post24/remote_service_' . $type);

        return $this;
    }

    /**
     * Return config type
     *
     * @return mixed
     */
    protected function _getType()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENDPOINT_TYPE, $this->_store);
    }

    /**
     * Return config url
     *
     * @return mixed
     */
    protected function _getUrl()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENDPOINT_URL, $this->_store);
    }

    /**
     * Set store
     *
     * @param $store
     *
     * @return $this
     */
    public function setStore($store)
    {
        $this->_store = $store;

        return $this;
    }

    /**
     * Return data
     *
     * @return bool|string
     */
    public function getData()
    {
        if (is_null($this->_service) || !$this->_service) {
            return false;
        }

        return $this->_service->setUrl($this->_getUrl())->get();
    }

}