<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Remote_Parser_Json
 */
class Scandi_Post24_Model_Remote_Parser_Json extends Scandi_Post24_Model_Remote_Parser_Abstract
{
    /**
     * @var array
     */
    protected $_allowedCountries = array('LV');

    /**
     * JSON file fields mapping
     *
     * @var array
     */
    protected $_dataMapping = array(
        'name' => 'NAME',
        'city' => 'A1_NAME',
        'address' => 'A2_NAME',
        'index' => 'ZIP',
        'place' => null
    );

    /**
     * JSON Data parser
     *
     * @return array
     */
    public function parse()
    {
        $result = array();
        $data = Mage::helper('core')->jsonDecode($this->_data);

        foreach ($data as $_item) {
            // Skip unnecessary countries
            if (!isset($_item['A0_NAME']) || !in_array($_item['A0_NAME'], $this->_allowedCountries)) {
                continue;
            }

            // Apply mapping
            $resultItem = array();
            foreach ($this->_dataMapping as $_resultKey => $_jsonKey) {
                $resultItem[$_resultKey] = ($_jsonKey && isset($_item[$_jsonKey])) ? $_item[$_jsonKey] : '';
            }

            $result[] = $resultItem;
        }

        return $result;
    }
}