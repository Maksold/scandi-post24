<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Remote_Parser_Abstract
 */
abstract class Scandi_Post24_Model_Remote_Parser_Abstract
{
    /**
     * @var null
     */
    protected $_data = null;

    /**
     * Setter for $_data
     *
     * @param string $data
     *
     * @return $this
     */
    public function setData($data = '')
    {
        $this->_data = $data;

        return $this;
    }

    /**
     * Data parser abstract method
     *
     * @return array
     */
    abstract public function parse();
}