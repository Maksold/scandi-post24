<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Remote_Parser
 */
class Scandi_Post24_Model_Remote_Parser
{
    /**
     *
     */
    CONST XML_PATH_ENDPOINT_DATA_TYPE = 'carriers/scandipost24/endpoint_data_type';

    /**
     * @var Scandi_Post24_Model_Remote_Parser_Abstract
     */
    protected $_parser = null;
    /**
     * @var string
     */
    protected $_data = '';
    /**
     * @var Mage_Core_Model_Store
     */
    protected $_store = null;

    /**
     *
     */
    public function __construct()
    {
        $this->_setParser();
    }

    /**
     * Set parser method
     *
     * @return $this
     */
    protected function _setParser()
    {
        $dataType = $this->_getDataType();

        if (!$dataType) {
            return $this;
        }

        $this->_parser = Mage::getModel('scandi_post24/remote_parser_' . $dataType);

        return $this;
    }

    /**
     * Return config data type
     *
     * @return mixed
     */
    protected function _getDataType()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENDPOINT_DATA_TYPE, $this->_store);
    }

    /**
     * Setter for $_data
     *
     * @param $data
     *
     * @return $this
     */
    public function setData($data)
    {
        $this->_data = $data;

        return $this;
    }

    /**
     * Set store
     *
     * @param $store
     *
     * @return $this
     */
    public function setStore($store)
    {
        $this->_store = $store;

        return $this;
    }

    /**
     * Return remote data as array
     *
     * @return array
     */
    public function getRemoteDataArray()
    {
        if (is_null($this->_parser) || !$this->_parser) {
            return array();
        }

        return $this->_parser->setData($this->_data)->parse();
    }
}