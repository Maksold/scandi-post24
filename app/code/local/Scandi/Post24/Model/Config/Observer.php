<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Config_Observer
 */
class Scandi_Post24_Model_Config_Observer
{
    /**
     * Path extension frontend layout xml file
     */
    const PATH_LAYOUT_XML_FILE = "frontend/layout/updates/scandi_post24/file";

    /**
     * Display extension frontend layout xml
     *
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function disableModule(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('scandi_post24')->isActive()) {
            Mage::getConfig()->setNode(self::PATH_LAYOUT_XML_FILE, null);
        }

        return $this;
    }

}