<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Terminals
 *
 * @method setId
 * @method getId
 * @method setStoreId
 * @method getStoreId
 * @method setName
 * @method getName
 * @method setCity
 * @method getCity
 * @method setAddress
 * @method getAddress
 * @method setIndex
 * @method getIndex
 * @method setPlace
 * @method getPlace
 * @method setCreatedAt
 * @method getCreatedAt
 */
class Scandi_Post24_Model_Terminals extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('scandi_post24/terminals');
    }

    /**
     * Save updatedAt and createdAt params
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $this->setCreatedAt(now());

        return parent::_beforeSave();
    }

    /**
     * Load terminal by index field
     *
     * @param $index
     *
     * @return $this
     */
    public function loadByIndex($index)
    {
        $this->_getResource()->loadByIndex($this, $index, Mage::app()->getStore()->getId());
        return $this;
    }

    /**
     * @param Mage_Core_Model_Store $store
     *
     * @return $this|array
     */
    public function loadFromRemote(Mage_Core_Model_Store $store)
    {
        $remoteData = $this->_getRemoteService()->setStore($store)->getData();
        if ($remoteData === false) {
            /**
             * Generate error
             */
            return $this;
        }

        $data = $this->_getRemoteParser()->setData($remoteData)->setStore($store)->getRemoteDataArray();
        if ($data === false || !count($data)) {
            /**
             * Generate error
             */
            return $this;
        }

        return $data;
    }

    /**
     * Delete collection by store
     *
     * @param Mage_Core_Model_Store $store
     *
     * @return $this
     */
    public function deleteCollectionByStore(Mage_Core_Model_Store $store)
    {
        $itemCollection = $this->getCollection()
            ->addFieldToFilter('store_id', array('eq', $store->getId()));

        foreach ($itemCollection as $item) {
            /**
             * @var $item Scandi_Post24_Model_Terminals
             */
            $item->delete();
        }

        return $this;
    }

    /**
     * Retrieve Scandi_Post24_Model_Remote_Parser
     *
     * @return Scandi_Post24_Model_Remote_Parser
     */
    protected function _getRemoteParser()
    {
        return Mage::getModel('scandi_post24/remote_parser');
    }

    /**
     * Retrieve Scandi_Post24_Model_Remote_Service
     *
     * @return Scandi_Post24_Model_Remote_Service
     */
    protected function _getRemoteService()
    {
        return Mage::getModel('scandi_post24/remote_service');
    }

}