<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Resource_Terminals_Collection
 */
class Scandi_Post24_Model_Resource_Terminals_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('scandi_post24/terminals');
    }

    /**
     * Add store to filter
     *
     * @param Mage_Core_Model_Store $store
     *
     * @return $this
     */
    public function addStoreFilter(Mage_Core_Model_Store $store)
    {
        $this->addFilter('`store_id`', $store->getId());

        return $this;
    }

    /**
     * Set website filer
     *
     * @param int $websiteId
     *
     * @return $this
     */
    public function setWebsiteFilter($websiteId)
    {
        $this->_select->where("store_id = ?", $websiteId);

        return $this;
    }

    /**
     * Add index field to filter
     *
     * @param $index
     *
     * @return $this
     */
    public function addIndexFilter($index)
    {
        $this->addFilter('`index`', $index);

        return $this;
    }
}