<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Model_Resource_Terminals
 */
class Scandi_Post24_Model_Resource_Terminals extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('scandi_post24/terminals', 'terminal_id');
    }

    /**
     * Load terminals by store
     *
     * @param Scandi_Post24_Model_Terminals $terminal
     * @param                               $index
     * @param                               $storeId
     *
     * @return $this
     */
    public function loadByIndex(Scandi_Post24_Model_Terminals $terminal, $index, $storeId)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
            ->from($this->getMainTable())
            ->where('`index` = ?', $index)
            ->where('`store_id` = ?', $storeId);

        $terminalId = $adapter->fetchOne($select);
        if ($terminalId) {
            $this->load($terminal, $terminalId);
        } else {
            $terminal->setData(array());
        }

        return $this;
    }
}
