<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Helper_Data
 */
class Scandi_Post24_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_SHIPPING_METHOD_NAME = 'carriers/scandipost24/name';
    const XML_PATH_SHIPPING_TITLE = 'carriers/scandipost24/title';
    const XML_PATH_SHIPPING_ACTIVE = 'carriers/scandipost24/active';

    /**
     * Return shipping method is active
     *
     * @return bool
     */
    public function isActive()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_SHIPPING_ACTIVE);
    }

    /**
     * Return shipping method name
     *
     * @return mixed
     */
    public function getShippingMethodName()
    {
        return Mage::getStoreConfig(self::XML_PATH_SHIPPING_METHOD_NAME);
    }

    /**
     * Return shipping title
     *
     * @return mixed
     */
    public function getShippingTitle()
    {
        return Mage::getStoreConfig(self::XML_PATH_SHIPPING_TITLE);
    }

    /**
     * Sort rates by title
     *
     * @param $rates
     *
     * @return array
     */
    public function sortRatesByTitle($rates)
    {
        if (!is_array($rates) || !count($rates)) {
            return $rates;
        }

        foreach ($rates as $i => $rate) {
            $tmp[$i] = $rate->getMethodTitle();
        }

        natsort($tmp);

        $result = array();
        foreach ($tmp as $i => $price) {
            $result[] = $rates[$i];
        }

        return $result;
    }
}