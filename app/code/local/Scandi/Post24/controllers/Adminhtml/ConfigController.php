<?php
/**
 * Scandi_Post24
 *
 * @category    Scandi
 * @package     Scandi_Post24
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2013 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Class Scandi_Post24_Adminhtml_ConfigController
 */
class Scandi_Post24_Adminhtml_ConfigController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Export shipping table rates in csv format
     */
    public function exportRatesAction()
    {
        $gridBlock = $this->getLayout()->createBlock('scandi_post24/adminhtml_shipping_carrier_post24_grid');
        /**
         * @var $gridBlock Scandi_Post24_Block_Adminhtml_Shipping_Carrier_Post24_Grid
         */
        $website  = Mage::app()->getWebsite($this->getRequest()->getParam('website'));
        $fileName = 'scandi_post24_' . $website->getCode() . '.csv';
        $gridBlock->setWebsiteId($website->getId());
        $content = $gridBlock->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

}